/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao.impl;

import com.dao.ProductDAO;
import com.db.DatabaseConnection;
import com.model.Category;
import com.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Latief
 */
public class ProductDAOImpl implements ProductDAO {

    private Connection connection;
    private static ProductDAOImpl productDAO;

    public ProductDAOImpl(Connection connection) {
        this.connection = connection;
    }

    /**
     * Gunakan fungsi ini untuk mendapatkan singleton dari <code>ProductDaoImpl</code>.
     * @return singleton dari <code>ProductDaoImpl</code>
     * @throws SQLException 
     */
    public static ProductDAO getInstance() throws SQLException {
        if (productDAO == null) {
            productDAO = new ProductDAOImpl(DatabaseConnection.getInstance().getConnection());
        }
        return productDAO;
    }

    @Override
    public int save(Product product) throws SQLException {
        String sql = "INSERT INTO product (name, description, price, expired_date, category_id) values(?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, product.getName());
        statement.setString(2, product.getDescription());
        statement.setDouble(3, product.getPrice());
        statement.setDate(4, new java.sql.Date(product.getExpiredDate().getTime()));
        statement.setInt(5, product.getCategory().getId());
        return statement.executeUpdate();
    }

    @Override
    public int update(int id, Product product) throws SQLException {
        String sql = "UPDATE product c SET name = ?, description = ?, price = ?, expired_date = ?, category_id = ? WHERE product_id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, product.getName());
        statement.setString(2, product.getDescription());
        statement.setDouble(3, product.getPrice());
        statement.setDate(4, new java.sql.Date(product.getExpiredDate().getTime()));
        statement.setInt(5, product.getCategory().getId());
        statement.setInt(6, id);
        return statement.executeUpdate();
    }

    @Override
    public int delete(int id) throws SQLException {
        String sql = "DELETE FROM product WHERE product_id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, id);
        return statement.executeUpdate();
    }

    @Override
    public Product findById(int id) throws SQLException {
        String sql =
                "SELECT p.product_id,p.`name`,p.description,p.price,p.`expired_date`,p.category_id,c.`name` as category_name "
                + " FROM product p INNER JOIN category c ON p.category_id = c.category_id where p.product_id like ? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        Product result = null;
        if (rs.next()) {
            Category c = new Category();
            c.setId(rs.getInt("category_id"));
            c.setName(rs.getString("category_name"));

            Product p = new Product();
            p.setCategory(c);
            p.setId(rs.getInt("product_id"));
            p.setName(rs.getString("name"));
            p.setDescription(rs.getString("description"));
            p.setPrice(rs.getDouble("price"));
            Calendar cal = Calendar.getInstance();
            p.setExpiredDate(rs.getDate("expired_date"));
            result = p;
        }
        return result;
    }

    @Override
    public List<Product> findAll() throws SQLException {
        String sql =
                "SELECT p.product_id,p.`name`,p.description,p.price,p.`expired_date`,p.category_id,c.`name` as category_name "
                + " FROM product p INNER JOIN category c ON p.category_id = c.category_id";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet rs = statement.executeQuery();
        List<Product> result = new ArrayList<Product>();
        while (rs.next()) {
            Category c = new Category();
            c.setId(rs.getInt("category_id"));
            c.setName(rs.getString("category_name"));

            Product p = new Product();
            p.setCategory(c);
            p.setId(rs.getInt("product_id"));
            p.setName(rs.getString("name"));
            p.setDescription(rs.getString("description"));
            p.setPrice(rs.getDouble("price"));
            p.setExpiredDate(new java.util.Date(rs.getDate("expired_date").getTime()));
            result.add(p);
        }
        return result;
    }

    @Override
    public List<Product> findByName(String name) throws SQLException {
        String sql =
                "SELECT p.product_id,p.`name`,p.description,p.price,p.`expired_date`,p.category_id,c.`name` as category_name "
                + " FROM product p INNER JOIN category c ON p.category_id = c.category_id where p.name like ? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, "%" + name + "%");
        List<Product> result = new ArrayList<Product>();
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            Category c = new Category();
            c.setId(rs.getInt("category_id"));
            c.setName(rs.getString("category_name"));

            Product p = new Product();
            p.setCategory(c);
            p.setId(rs.getInt("product_id"));
            p.setName(rs.getString("name"));
            p.setDescription(rs.getString("description"));
            p.setPrice(rs.getDouble("price"));
            p.setExpiredDate(rs.getDate("expired_date"));

            result.add(p);
        }
        return result;
    }
}
