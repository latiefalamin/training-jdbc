/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.main;

import com.model.Category;
import com.view.Frame;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Latief
 */
public class Main {

    /**
     * hello
     *
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Frame frame = new Frame();
                frame.setVisible(true);
            }
        });

    }
//    public static void main(String[] args) {
//
//        try {
//            CategoryDAO categoryDAO = new CategoryDAOImpl(DatabaseConnection.getInstance().getConnection());
//            ProductDAO productDAO = new ProductDAOImp(DatabaseConnection.getInstance().getConnection());
//
//            Category category = new Category();
//            category.setName("Test");
//            categoryDAO.save(category);
//
//            System.out.println("****SAVE**********************");
//            List<Category> categorys = categoryDAO.findAll();
//            showCategories(categorys);
//
//            System.out.println("****UPDATE*********************");
//            Category categoryUpdated = categorys.get(0);
//            categoryUpdated.setName("Updated Broo...");
//            categoryDAO.update(categoryUpdated.getId(), categoryUpdated);
//            categorys = categoryDAO.findAll();
//            showCategories(categorys);
//
//            System.out.println("****DELETE********************");
//            categoryDAO.delete(categorys.get(0).getId());
//            categorys = categoryDAO.findAll();
//            showCategories(categorys);
//        } catch (SQLException ex) {
//            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

    public static void showCategories(List<Category> categorys) {
        for (Category c : categorys) {
            System.out.println("------------------------------------------------");
            showCategory(c);
        }
    }

    public static void showCategory(Category category) {
        System.out.println("Category Id : " + category.getId());
        System.out.println("Category Name : " + category.getName());
    }
}
