/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.view;

import com.dao.CategoryDAO;
import com.dao.impl.CategoryDAOImpl;
import com.model.Category;
import com.util.CostumTableModel;
import com.util.SwingUtil;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Latief
 */
public class InternalFrameCategory extends javax.swing.JInternalFrame {

    /**
     * Creates new form InternalFrameCategory
     */
    public InternalFrameCategory() {
        initComponents();
        //Inisialisasi categoryDAO
        try {
            categoryDAO = CategoryDAOImpl.getInstance();
        } catch (SQLException ex) {
            Logger.getLogger(InternalFrameCategory.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, ex.getMessage(), "SQLException", JOptionPane.ERROR_MESSAGE);
        }
        reloadCategories();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        toolBar = new javax.swing.JToolBar();
        buttonAdd = new javax.swing.JButton();
        buttonDelete = new javax.swing.JButton();
        buttonReload = new javax.swing.JButton();
        scrollPane = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        setClosable(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        setForeground(java.awt.Color.white);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Category");

        toolBar.setFloatable(false);

        buttonAdd.setText("Add");
        buttonAdd.setFocusable(false);
        buttonAdd.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddActionPerformed(evt);
            }
        });
        toolBar.add(buttonAdd);

        buttonDelete.setText("Delete");
        buttonDelete.setFocusable(false);
        buttonDelete.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonDelete.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDeleteActionPerformed(evt);
            }
        });
        toolBar.add(buttonDelete);

        buttonReload.setText("Reload");
        buttonReload.setFocusable(false);
        buttonReload.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonReload.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonReloadActionPerformed(evt);
            }
        });
        toolBar.add(buttonReload);

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        scrollPane.setViewportView(table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(toolBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(toolBar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddActionPerformed
        if (dialog == null) {//jika DialogCategory masih null, diinisialisasikan
            dialog = new DialogCategory(SwingUtil.getParentJFrame(this), true);
        }
        dialog.setCategory(new Category());//buat object Category kosong
        dialog.setVisible(true);
        //Jika disimpan Category barunya -SAVE-
        if (DialogCategory.SAVE_MODE == dialog.getMode()) {
            Category category = dialog.getCategory();//ambil Category baru dari DialogCategory
            try {
                categoryDAO.save(category);//simpan Category baru
            } catch (SQLException ex) {
                Logger.getLogger(InternalFrameCategory.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, ex.getMessage(), "SQLException", JOptionPane.ERROR_MESSAGE);
            }
            reloadCategories();//reload
        }
    }//GEN-LAST:event_buttonAddActionPerformed

    private void buttonReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonReloadActionPerformed
        reloadCategories();
    }//GEN-LAST:event_buttonReloadActionPerformed

    private void buttonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDeleteActionPerformed
        int id = (Integer) table.getValueAt(table.getSelectedRow(), 0);//ambil Category yang disorot
        try {
            categoryDAO.delete(id);//hapus Category berdasarkan id
        } catch (SQLException ex) {
            Logger.getLogger(InternalFrameCategory.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, ex.getMessage(), "SQLException", JOptionPane.ERROR_MESSAGE);
        }
        reloadCategories();//reload
    }//GEN-LAST:event_buttonDeleteActionPerformed

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (2 == evt.getClickCount()) { //jika double clik
            if (dialog == null) {//jika DialogCategory masih null, diinisialisasikan
                dialog = new DialogCategory(SwingUtil.getParentJFrame(this), true);
            }

            //Ambil id Category yang di double click
            int id = (Integer) table.getValueAt(table.getSelectedRow(), 0);
            Category category = null;
            try {
                category = categoryDAO.findByid(id); //cari Category dari database berdasarkan id
            } catch (SQLException ex) {
                Logger.getLogger(InternalFrameCategory.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, ex.getMessage(), "SQLException", JOptionPane.ERROR_MESSAGE);
                return;
            }
            dialog.setCategory(category); //DialogCategory diisikan categorynya
            dialog.setVisible(true); //DialogCategory tampil

            //jika perubahan Category di simpan -SAVE-
            if (DialogCategory.SAVE_MODE == dialog.getMode()) {
                category = dialog.getCategory();//perubahan nilai Category, di packing di object Category
                try {
                    categoryDAO.update(category.getId(), category); //simpan perubahan Category
                } catch (SQLException ex) {
                    Logger.getLogger(InternalFrameCategory.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "SQLException", JOptionPane.ERROR_MESSAGE);
                }
                reloadCategories();//reload
            }
        }
    }//GEN-LAST:event_tableMouseClicked
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAdd;
    private javax.swing.JButton buttonDelete;
    private javax.swing.JButton buttonReload;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JTable table;
    private javax.swing.JToolBar toolBar;
    // End of variables declaration//GEN-END:variables
    private DialogCategory dialog;
    private CategoryDAO categoryDAO;

    /**
     * Fungsi ini me-reload data Category dari database yang tampil di table.
     */
    public void reloadCategories() {
        List<Category> categories = null;//List tampungan daftar Category
        try {
            categories = categoryDAO.findAll();//ambil semua Category dari database
        } catch (SQLException ex) {
            Logger.getLogger(InternalFrameCategory.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, ex.getMessage(), "SQLException", JOptionPane.ERROR_MESSAGE);
        }

        int row = (categories == null ? 0 : categories.size());//hitung berapa baris
        int col = 3;//kolom pasti 
        //Data yang ditampilkan pada JTable dalam bentuk array 2 dimensi
        Object[][] data = new Object[row][col];

        //Konversi List<Category> ke dalam array 2 dimensi
        for (int i = 0; categories != null && i < categories.size(); i++) {
            Category category = categories.get(i);
            data[i][0] = category.getId();
            data[i][1] = category.getName();
            data[i][2] = Boolean.FALSE;
        }

        //Column header pada JTable
        String[] columnNames = {"Id", "Name", "Centang"};
        //Column yang dapat diedit pada JTabel
        boolean[] canEdit = new boolean[]{false, false, false};
        //Buat CostumTableModel dan tampilkan di JTable
        CostumTableModel tableModel = new CostumTableModel(data, columnNames);
        tableModel.setCanEdit(canEdit);
        table.setModel(tableModel);
    }
}
