/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Latief
 */
public class CostumTableModel extends DefaultTableModel {

    private Object[][] data = null;
    private boolean[] canEdit = null;

    public CostumTableModel(Object[][] data, Object[] columnNames) {
        super(data, columnNames);
        this.data = data;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return canEdit == null ? false : canEdit[column];
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return getRowCount() == 0 ? Object.class : getValueAt(0, columnIndex).getClass();
    }

    public boolean[] getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean[] canEdit) {
        this.canEdit = canEdit;
    }
}
