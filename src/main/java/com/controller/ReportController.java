/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Latief
 */
public final class ReportController {

    /**
     * Tampilkan report
     *
     * @param fileReport path template report, jasper
     * @param parameter parameter untuk report
     * @param dataSource datasource yang akan di jadikan report
     * @param judul judul dari window preview nya
     */
    public static void previewReport(String fileReport, Map parameter, List dataSource, String judul) throws JRException {
        JasperPrint jasperPrint = JasperFillManager.fillReport(
                fileReport,
                parameter,
                new JRBeanCollectionDataSource(dataSource));
        JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
        jasperViewer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jasperViewer.setTitle(judul);
        jasperViewer.setVisible(true);
    }

    /**
     * Cetak Report
     *
     * @param fileReport path template report, jasper
     * @param parameter parameter untuk report
     * @param dataSource datasource yang akan di jadikan report
     */
    public static void printReport(String fileReport, Map parameter, List dataSource) throws JRException {
        JasperPrint jasperPrint = JasperFillManager.fillReport(
                fileReport,
                parameter,
                new JRBeanCollectionDataSource(dataSource));
        JasperPrintManager.printReport(jasperPrint, true);
    }
}
